public class stringsBuffers {

    public static void main(String[] args) {
        String str1 = "Hello";
        StringBuffer strbuf1 = new StringBuffer("World");

        System.err.println(strbuf1.capacity());

        System.out.println("str1: " + str1); // Hello
        System.out.println("strbuf1: " + strbuf1); // World

        /* Appending to a string */
        String appStr1 = str1 + " " + strbuf1;
        System.out.println("appStr1: " + appStr1); // Hello World
    }
}