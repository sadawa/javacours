Les opérateurs d'assignation en Java sont utilisés pour attribuer une valeur à une variable. Voici une explication des opérateurs d'assignation les plus courants avec des exemples pour chacun :

Opérateur d'assignation simple (=) :
Cet opérateur attribue une valeur à une variable.

Exemple :

java

int a;
a = 5; // la valeur 5 est assignée à la variable 'a'
Opérateur d'addition et assignation (+=) :
Cet opérateur ajoute la valeur à droite à la valeur de la variable à gauche, puis attribue le résultat à la variable.

Exemple :

java

int a = 5;
a += 3; // équivaut à a = a + 3;
// Maintenant, la valeur de 'a' est 8
Opérateur de soustraction et assignation (-=) :
Cet opérateur soustrait la valeur à droite de la valeur de la variable à gauche, puis attribue le résultat à la variable.

Exemple :

java

int a = 10;
a -= 4; // équivaut à a = a - 4;
// Maintenant, la valeur de 'a' est 6
Opérateur de multiplication et assignation (*=) :
Cet opérateur multiplie la valeur à droite par la valeur de la variable à gauche, puis attribue le résultat à la variable.

Exemple :

java

int a = 3;
a *= 2; // équivaut à a = a * 2;
// Maintenant, la valeur de 'a' est 6
Opérateur de division et assignation (/=) :
Cet opérateur divise la valeur de la variable à gauche par la valeur à droite, puis attribue le résultat à la variable.

Exemple :

java

int a = 10;
a /= 2; // équivaut à a = a / 2;
// Maintenant, la valeur de 'a' est 5
Opérateur de modulo et assignation (%=) :
Cet opérateur effectue le modulo de la valeur de la variable à gauche par la valeur à droite, puis attribue le résultat à la variable.

Exemple :

java

int a = 13;
a %= 5; // équivaut à a = a % 5;
// Maintenant, la valeur de 'a' est 3

 Les opérateurs logiques en Java sont utilisés pour effectuer des opérations logiques sur les valeurs booléennes. Voici une explication des principaux opérateurs logiques avec des exemples pour chacun :

Opérateur ET (&&) :
Cet opérateur renvoie vrai (true) si les deux expressions booléennes sur lesquelles il opère sont vraies.

Exemple :

java

boolean a = true;
boolean b = false;
boolean resultat = (a && b); // resultat sera false car une des expressions est fausse
Opérateur OU (||) :
Cet opérateur renvoie vrai (true) si au moins l'une des expressions booléennes sur lesquelles il opère est vraie.

Exemple :

java

boolean a = true;
boolean b = false;
boolean resultat = (a || b); // resultat sera true car au moins une des expressions est vraie
Opérateur NON (!) :
Cet opérateur inverse la valeur d'une expression booléenne. Si l'expression est vraie, il renvoie faux (false), et vice versa.

Exemple :

java

boolean a = true;
boolean resultat = !a; // resultat sera false car a est vrai et !a est faux