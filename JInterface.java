//class -> class -> extends
//class -> interface -> implements
//interface -> interface -> extends

/**
 * InnerInferfacej
 */
interface A {

    int age = 25;
    String area = "Paris";

    void show();

    void config();

}

interface X {
    void run();
}

interface Y extends X {

}

class B implements A, X {
    public void show() {
        System.out.println("IN Show");
    }

    public void config() {
        System.out.println("IN Config");
    }

    public void run() {
        System.out.println("IN Run");
    }
}

public class JInterface {
    public static void main(String[] args) {
        A obj;
        System.out.println(A.age + " " + A.area + " " + "Done");

        obj = new B();
        obj.show();
        obj.config();

        X obj1 = new B();
        obj1.run();

    }
}
