import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ArrayListj {
    public static void main(String[] args) {
        Collection<Integer> nums = new ArrayList<Integer>();
        nums.add(6);
        nums.add(4);
        nums.add(8);
        nums.add(9);

        for (int n : nums) {

            System.out.println(n);

        }

        // MAP
        Map<String, Integer> students = new HashMap<String, Integer>();
        students.put("Shiro", 20);
        students.put("Julie", 35);
        students.put("John", 45);
        students.put("Ellie", 25);

        System.out.println(students);
    }
}
