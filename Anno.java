class A {
    public void showTheData() {
        System.out.println("IN Show");
    }
}

class B extends A {

    @Override
    public void showTheData() {
        System.out.println("IN Show B");
    }
}

public class Anno {
    public static void main(String a[]) {
        B obj = new B();
        obj.showTheData();
    }
}
