class A {

    int age;

    public void show() {
        System.out.println("Show");
    }

    class B {
        public void config() {
            System.out.println("config");
        }
    }
}

class C {
    public void show() {
        System.out.println("Show C");

    }
}

abstract class D {
    public abstract void show();
}

public class IneerClassj {
    public static void main(String[] args) {
        A obj = new A();
        obj.show();

        // Option If one class is static so A.B obj = new A.B();
        A.B obj1 = obj.new B();
        obj1.config();

        // Anonymous Inner class
        C obj2 = new C() {
            public void show() {
                System.out.println(" new Show ");

            }
        };
        obj2.show();

        // abtract class and anonymous inner class
        D obj3 = new D() {
            public void show() {
                System.out.println(" new Show ");
            }
        };
        obj3.show();
    }
}
