import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Input_Buffered {

    public static void main(String[] args) throws IOException {
        System.out.print("Enter a number: ");

        int num = System.in.read();

        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader bf = new BufferedReader(in);

        System.out.println(num - 48);

        int num2 = Integer.parseInt(bf.readLine());
        System.out.println(num2);

        System.out.print("Enter another number: ");
        try (Scanner sca = new Scanner(System.in)) {
            int num3 = sca.nextInt();
            System.out.println(num3);
        }

        bf.close();
    }
}