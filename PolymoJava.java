class A {
    public void show() {
        System.out.println("IN Show");
    }
}

class B extends A {
    public void show() {
        System.out.println("IN Show B");
    }
}

class C extends A {
    public void show() {
        System.out.println("IN Show C");
    }
}

public class PolymoJava {
    public static void main(String[] args) {
        A obj = new A();
        obj.show();

        obj = new B();
        obj.show();

        obj = new C();
        obj.show();
    }
}