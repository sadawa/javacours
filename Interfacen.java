class Computer {
    public void code() {

    }
}

class Laptop extends Computer {
    public void code() {
        System.out.println("code,compile,run");
    }
}

class Desktop extends Computer {
    public void code() {
        System.out.println("code,compile,run: Faster");
    }
}

class Developer {
    public void devApp(Computer lap) {
        lap.code();
    }
}

public class Interfacen {
    public static void main(String[] args) {
        Computer lap = new Laptop();
        Computer desk = new Desktop();

        lap.code();
        desk.code();

    }
}
