class Human {
    private int age;
    private String name;

    public Human() {
        this.age = 25;
        this.name = "John Doe";
    }

    public int setAge(int age) {
        this.age = age;
        return age;
    }

    public int getAge() {
        return age;
    }

    public String setName(String name) {
        this.name = name;
        return name;
    }

    public String getName() {
        return name;
    }
}

public class encaps {

    public static void main(String[] args) {
        Human obj = new Human();
        System.out.println(obj.getName() + " " + obj.getAge());

        obj.setAge(25);
        obj.setName("Shiro");
        // obj.age = 20;
        // obj.name = "Shiro";
        System.out.println(obj.getName() + " " + obj.getAge());
    }
}
