import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Steamj {
    public static void main(String[] args) {

        List<Integer> nums = Arrays.asList(5, 8, 6, 8, 10);

        // Stream

        Stream<Integer> s1 = nums.stream();
        Stream<Integer> s2 = nums.stream();
        s2.map(n -> n * 2);

        s1.forEach(n -> System.out.println(s1));
        s2.forEach(n -> System.out.println(n));

        nums.forEach(n -> System.out.println(n));

        System.out.println(nums);

        // Antoher option
        // List<Integer> nums = new ArrayList<>()
    }
}
