@FunctionalInterface
interface A {
    void show();

}

@FunctionalInterface
interface B {
    int add(int i, int j);

}

public class funcInterface {
    public static void main(String[] args) {
        // A obj = new B();
        // obj.show();

        // Lambda Expression
        A obj = () -> {
            System.out.println("IN Show");
        };
        obj.show();

        // B obj2 = new B() {
        // public int add(int i, int j) {
        // return i + j;
        // }
        // };

        // Lambda Expression
        B obj2 = (i, j) -> i + j;
        int result = obj2.add(5, 4);
        System.out.println(result);

    }

}
