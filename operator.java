public class operator {
    public static void main(String[] args) {
        // Operator
        int num = 3;
        int num2 = 5;
        int num3 = 3;
        int num4 = 8;
        int result = num + num2; // addition num += num2
        int result2 = num - num2; // subtraction num -= num2
        int result3 = num * num2; // multiplication num *= num2
        int result4 = num / num2; // division num /= num2
        int result5 = num % num2; // modulus num %= num2
        // relationnal operators
        boolean result6 = num > num2; // greater than num > num2
        boolean result7 = num < num2; // less than num < num2
        boolean result8 = num >= num2; // greater than or equal to num >= num2
        boolean result9 = num <= num2; // less than or equal to num <= num2
        boolean result10 = num == num3; // equal to num == num2
        // Logical operator
        boolean result11 = num < num2 && num3 > num4; // logical AND num < num2 && num3 > num4
        boolean result12 = num < num2 || num3 > num4; // logical OR num < num2 || num3 > num4
        boolean result13 = num < num2 && num3 > num4 || num < num2 || num3 > num4; // logical XOR num < num2 && num3 >
                                                                                   // num4 || num < num2 || num3 > num4
        // print operator
        System.out.println(result);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
        System.out.println(result5);
        System.out.println(result6);
        System.out.println(result7);
        System.out.println(result8);
        System.out.println(result9);
        System.out.println(result10);
        System.out.println(result11);
        System.out.println(result12);
        System.out.println(result13);
    }
}
