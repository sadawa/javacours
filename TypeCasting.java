class A {
    public void show() {
        System.out.println("Show");
    }
}

class B extends A {
    public void show2() {
        System.out.println("Show2");
    }
}

public class TypeCasting {
    public static void main(String a[]) {
        // Typecasting methods
        double d = 4.5;
        int i = (int) d;

        System.out.println(i);

        // Upcasting methods
        A obj = (A) new B();
        obj.show();

        // Downcasting methods
        B obj1 = (B) obj;
        obj1.show2();
    }
}
