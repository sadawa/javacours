class Calculator {
    int a;

    public int add(int n1, int n2) {
        int r = n1 + n2;
        return r;
    }
}

class Computer {
    public void playMusic() {
        System.out.println("Playing music ..");
    }

    public String getMePen(int cost) {
        return "Pen";
    }
}

public class Demos {
    public static void main(String a[]) {
        int num1 = 4;
        int num2 = 5;

        // add construtor
        Calculator calc = new Calculator();
        Computer obj = new Computer();

        obj.playMusic();
        String str = obj.getMePen(10);

        int result = calc.add(num1, num2);

        System.out.println("The sum is :" + result);
        System.out.println("The pen is: " + str);

    }
}
