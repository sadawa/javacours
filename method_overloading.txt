Method Overloading (surcharge de méthode) en Java est la possibilité de définir plusieurs méthodes ayant le même nom, mais avec des paramètres différents dans la même classe. Cela permet de fournir plusieurs façons d'appeler une méthode en fonction des arguments passés.

Dans Method Overloading, les méthodes doivent avoir des signatures différentes, c'est-à-dire qu'elles doivent avoir un nombre différent de paramètres, ou des types de paramètres différents, ou les deux. Le retour de la méthode ne fait pas partie de la signature de la méthode, donc il ne peut pas être utilisé pour surcharger une méthode.

Voici un exemple de Method Overloading :


public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }

    public double add(double a, double b) {
        return a + b;
    }

    public int add(int a, int b, int c) {
        return a + b + c;
    }
}
Dans cet exemple, nous avons trois méthodes add dans la classe Calculator. La première méthode prend deux paramètres de type int et renvoie un int. La deuxième méthode prend deux paramètres de type double et renvoie un double. La troisième méthode prend trois paramètres de type int et renvoie un int.

Lorsque nous appelons la méthode add, Java sélectionne la méthode appropriée en fonction des arguments passés. Par exemple :


Calculator calc = new Calculator();
int result1 = calc.add(2, 3); // appelle la première méthode add
double result2 = calc.add(2.5, 3.5); // appelle la deuxième méthode add
int result3 = calc.add(2, 3, 4); // appelle la troisième méthode add
Dans le premier appel, nous passons deux entiers à la méthode add, donc Java sélectionne la première méthode add. Dans le deuxième appel, nous passons deux nombres à virgule flottante, donc Java sélectionne la deuxième méthode add. Dans le troisième appel, nous passons trois entiers, donc Java sélectionne la troisième méthode add.

En résumé, Method Overloading est utile lorsque nous voulons fournir plusieurs façons d'appeler une méthode en fonction des arguments passés. Cela permet d'écrire un code plus lisible et plus facile à utiliser.