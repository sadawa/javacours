class Hello {
    public static void main(String a[]) {
        // Create variable
        int num = 3;
        int num2 = 5;
        int result = num + num2;

        // Literal
        int num3 = 3;
        System.out.println(num3);

        // Type conversions
        byte b = 127;
        int ab = b;
        int c = 127;
        byte d = (byte) c;
        System.out.println(b);
        System.out.println(ab);
        System.out.println(d);

        // print hello world
        System.out.println("Hello world");

        // print result
        System.out.println(result);

        byte a1 = 10;
        byte a2 = 30;

        int result1 = a1 * a2;

        System.err.println(result1);

    }

}