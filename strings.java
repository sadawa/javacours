public class strings {
    public static void main(String[] args) {
        // String immutable
        String str1 = "Hello";
        String str2 = "World!";

        System.out.println("The concatenation of 'str1' and 'str2' is: ");

        // String mutable
        StringBuffer sb = new StringBuffer("Hello world ");
        System.out.println(sb);

        System.out.println(str1);
        System.out.println(str2);
    }
}
