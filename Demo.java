public class Demo {
    public static void main(String[] args) {
        // condition if statement
        int x = 18;
        int y = 10;

        int a = 20;
        int b = 30;
        int c = 40;
        if (x > 10 && y < 15) {
            System.out.println("Hello");
        } else {
            System.out.println("Bye");
        }

        if (a < x) {
            System.out.println("a is greater than x");
        } else if (b < x) {
            System.out.println("b is greater than x");
        } else if (c < x) {
            System.out.println("c is greater than x");
        } else {
            System.out.println("x is greater than a, b, and c");
        }

        // Condition ternaire
        int num = 10;
        String result = num > 10 ? "num is greater than 10" : "num is not greater than 10";
        System.out.println(result);

        // Switch
        int d = 8;
        switch (d) {
            case 1:
                System.out.println("lundi");
                break;
            case 2:
                System.out.println("mardi");
                break;
            case 3:
                System.out.println("mercredi");
                break;
            case 4:
                System.out.println("jeudi");
                break;
            case 5:
                System.out.println("vendredi");
                break;
            case 6:
                System.out.println("samedi");
                break;
            case 7:
                System.out.println("dimanche");
                break;
            default:
                System.out.println("prend tous les jours  de la semaine");
        }
        // Boucle
        // while
        int i = 0;
        while (i <= 10) {
            System.out.println("yo" + " " + i);
            i++;
        }
        // do while
        int j = 0;
        do {
            System.out.println("Bonjour" + " " + j);
            j++;
        } while (j <= 10);

        // for
        for (int k = 0; k <= 10; k++) {
            System.out.println("Hi" + " " + k);
        }
    }

}
