En Java, une interface est un ensemble de méthodes abstraites qui définissent un contrat pour une fonctionnalité spécifique. Les classes qui implémentent une interface doivent fournir une implémentation pour toutes les méthodes abstraites définies dans l'interface.

Voici un exemple d'une interface en Java :


public interface Shape {
    double PI = 3.14; // une constante

    void draw(); // une méthode abstraite

    double area(); // une autre méthode abstraite
}
Dans cet exemple, nous avons défini une interface "Shape" qui contient une constante "PI" et deux méthodes abstraites "draw()" et "area()". Les classes qui implémentent cette interface doivent fournir une implémentation pour ces deux méthodes.

Voici un exemple d'une classe qui implémente l'interface "Shape" :


public class Circle implements Shape {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public void draw() {
        System.out.println("Drawing a circle");
    }

    public double area() {
        return PI * radius * radius;
    }
}
Dans cet exemple, nous avons défini une classe "Circle" qui implémente l'interface "Shape". La classe "Circle" fournit une implémentation pour les deux méthodes abstraites "draw()" et "area()" définies dans l'interface "Shape". Notez que la constante "PI" définie dans l'interface "Shape" est également accessible dans la classe "Circle" en utilisant le nom de l'interface.

En résumé, une interface en Java est un ensemble de méthodes abstraites qui définissent un contrat pour une fonctionnalité spécifique. Les classes qui implémentent une interface doivent fournir une implémentation pour toutes les méthodes abstraites définies dans l'interface. Les interfaces peuvent également contenir des constantes et des méthodes par défaut à partir de Java 8.